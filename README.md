#Velvet docker image

This repository contains Dockerfile that packages Velvet 1.2.10 Docker image.

Velvet 1.2.10 can be downloaded and its documentation can be found [here](https://www.ebi.ac.uk/~zerbino/velvet/).

Assembler was build using OPENMP parameter during compilation, all other compilation parameters were set to their defaults. 

### The recommended way to build an image is:

```
docker build -t velvet:1.2.10 .
```

### To run Velvet 1.2.10 in container:
If we have reads file <name>.fa in directory ~/work_dir we can prepare it with velveth and output results in ~/work_dir/output_directory:
```
docker run -v ~/work_dir:/mnt/ -u $(echo $UID) --rm parseq/velvet:1.2.10 ./velveth /mnt/output_directory 21 -short /mnt/<name>.fa
```
If <name>.fa was single reads file, when we can produce contigs by running:

```
docker run -v ~/work_dir:/mnt/ -u $(echo $UID) --rm parseq/velvet:1.2.10 ./velvetg /mnt/output_directory/
```
Resulting contigs will be in ~/work_dir/output_directory on host.
